package com.dreamerstrendyol.usermerchant.Exceptions;

import com.couchbase.client.core.error.CasMismatchException;
import com.couchbase.client.core.error.DocumentNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;
import java.util.NoSuchElementException;

@ControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler({UserDoesNotHaveTheItem.class})
    public ResponseEntity<Object> nullPointerException(UserDoesNotHaveTheItem exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("UserDoesNotHaveTheItem",
                "You trying to sell item but user dont have.",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({TryingToBuySoldItemOnMerchant.class})
    public ResponseEntity<Object> nullPointerException(TryingToBuySoldItemOnMerchant exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("TryingToBuySoldItemOnMerchant",
                "You trying to buy sold item.",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({TryingToDeleteSoldItemOnMerchant.class})
    public ResponseEntity<Object> nullPointerException(TryingToDeleteSoldItemOnMerchant exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("TryingToBuySoldItemOnMerchant",
                "You trying to delete sold item.",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({BalanceIsNotEnoughtForOperation.class})
    public ResponseEntity<Object> nullPointerException(BalanceIsNotEnoughtForOperation exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("BalanceIsNotEnoughtForOperation",
                "You trying to buy item but balance is not enough.",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({TryingToSellItemButAlreadyListed.class})
    public ResponseEntity<Object> nullPointerException(TryingToSellItemButAlreadyListed exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("TryingToSellItemButAlreadyListed",
                "You trying to sell item but it is already listed on merchant.",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({NullPointerException.class})
    public ResponseEntity<Object> nullPointerException(NullPointerException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("NullPointerException",
                "You trying to make request null pointer target",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({NoSuchElementException.class})
    public ResponseEntity<Object> noSuchElementException(NoSuchElementException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("NoSuchElementException",
                "Server can't find the request id.",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({CasMismatchException.class})
    public ResponseEntity<Object> documentNotFoundException(CasMismatchException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("CasMismatchException",
                "Document has been concurrently modified on the server.",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({DocumentNotFoundException.class})
    public ResponseEntity<Object> documentNotFoundException(DocumentNotFoundException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("DocumentNotFoundException",
                "There is no document as identified with this id.",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({HttpMessageNotReadableException.class})
    public ResponseEntity<Object> httpMessageNotReadableException(HttpMessageNotReadableException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("HttpMessageNotReadableException",
                "Request can't be accepted.",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ResourceAccessException.class})
    public ResponseEntity<Object> resourceAccessException(ResourceAccessException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("ResourceAccessException",
                "Server can't access data please try again.",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<Object> methodArgumentNotValidException(MethodArgumentNotValidException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("MethodArgumentNotValidException",
                "Some Fields need to ",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({HttpMediaTypeNotSupportedException.class})
    public ResponseEntity<Object> httpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("HttpMediaTypeNotSupportedException",
                "Please try to change request header as application/JSON",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }
}
