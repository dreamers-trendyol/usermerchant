package com.dreamerstrendyol.usermerchant.Models;

public enum ItemStatuses {
    IN_SALE,
    LOCKED_FOR,
    SOLD
}
