package com.dreamerstrendyol.usermerchant.Models;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@NoArgsConstructor
public class MarketItem {
    private String itemId;
    private String sellingId;
    private double itemPrice;
    private String ownerId;
    private ItemStatuses itemStatus;
    private Long version;

    public MarketItem(String itemId, double itemPrice, String ownerId) {
        this.itemId = itemId;
        this.sellingId = UUID.randomUUID().toString();
        this.itemPrice = itemPrice;
        this.ownerId = ownerId;
        this.itemStatus = ItemStatuses.IN_SALE;
    }

    public void makeMarketItemSold() {
        this.itemStatus = ItemStatuses.SOLD;
    }
}
