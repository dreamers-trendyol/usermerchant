package com.dreamerstrendyol.usermerchant.Models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Item implements Serializable {
    private String itemId;
    private String itemName;
    private String itemGameId;
    private String itemIcon;
    private String itemDescription;

    public Item(String itemName, String itemGameId, String itemIcon, String itemDescription) {
        this.itemId = UUID.randomUUID().toString();
        this.itemName = itemName;
        this.itemGameId = itemGameId;
        this.itemIcon = itemIcon;
        this.itemDescription = itemDescription;
    }
}
