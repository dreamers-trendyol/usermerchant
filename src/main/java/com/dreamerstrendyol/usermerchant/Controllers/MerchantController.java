package com.dreamerstrendyol.usermerchant.Controllers;

import com.dreamerstrendyol.usermerchant.Contracts.Requests.SellRequestDTO;
import com.dreamerstrendyol.usermerchant.Models.Item;
import com.dreamerstrendyol.usermerchant.Models.MarketItem;
import com.dreamerstrendyol.usermerchant.Services.UserMerchantService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AccessLevel;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/v1/user-merchant/merchant")
public class MerchantController {

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    UserMerchantService userMerchantService;

    @GetMapping("/")
    public ResponseEntity getMarketWithFilter(@RequestParam(required = true, value = "popular") String filter) {
        return ResponseEntity.ok(userMerchantService.getMarketWithFilter(filter));
    }

    @GetMapping("/{itemId}/merchant")
    public ResponseEntity<List<MarketItem>> getItemMerchant(@PathVariable String itemId,
                                                            @RequestParam(required = false, value = "in-sale") String filter) {
        List<MarketItem> itemMerchant = userMerchantService.getItemMerchant(itemId,filter);
        return ResponseEntity.ok(itemMerchant);
    }

    @PostMapping("/{itemId}/buy/{itemSellingId}/")
    public ResponseEntity<List<Item>> buyItemFromMerchant(@PathVariable String itemId,
                                                          @PathVariable String itemSellingId,
                                                          @RequestBody String userId) {
        userMerchantService.buyItemFromMerchant(itemId, itemSellingId, userId);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{itemId}/sell")
    public ResponseEntity sellItemOnMerchant(@PathVariable String itemId, @Valid @RequestBody SellRequestDTO sellRequestDTO) {
        userMerchantService.sellItemOnMerchant(itemId, sellRequestDTO);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{itemId}/merchant/{itemSellingId}")
    public ResponseEntity getItemIdBySellingId(@PathVariable String itemId, @PathVariable String itemSellingId) {
        return ResponseEntity.ok(userMerchantService.getItemIdBySellingId(itemId, itemSellingId));
    }

    @DeleteMapping("/{itemId}/merchant/{itemSellingId}")
    public ResponseEntity deleteItemIdBySellingId(@PathVariable String itemId, @PathVariable String itemSellingId) {
        userMerchantService.deleteItemIdBySellingId(itemId, itemSellingId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{itemId}/merchant/{itemSellingId}/owner")
    public ResponseEntity getOwnerIdBySellingId(@PathVariable String itemId, @PathVariable String itemSellingId) {
        return ResponseEntity.ok(userMerchantService.getOwnerIdBySellingId(itemId, itemSellingId));
    }


}
