package com.dreamerstrendyol.usermerchant.Controllers;

import com.dreamerstrendyol.usermerchant.Services.UsersService;
import lombok.AccessLevel;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/user-merchant/users")
public class UsersController {

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    UsersService usersService;

    @GetMapping("/{userId}")
    public ResponseEntity getUserListedItemsOnMerchant(@PathVariable String userId,
                                                       @RequestParam(required = false, defaultValue = "in-sale") String filter) {
        return ResponseEntity.ok(usersService.getUserListedItemsOnMerchant(userId, filter));
    }
}
