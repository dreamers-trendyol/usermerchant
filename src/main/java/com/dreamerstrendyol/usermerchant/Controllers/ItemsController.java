package com.dreamerstrendyol.usermerchant.Controllers;

import com.dreamerstrendyol.usermerchant.Contracts.Requests.AddItemRequest;
import com.dreamerstrendyol.usermerchant.Contracts.Requests.UpdateItemRequest;
import com.dreamerstrendyol.usermerchant.Helper.PatchHelper;
import com.dreamerstrendyol.usermerchant.Models.Item;
import com.dreamerstrendyol.usermerchant.Services.ItemsService;
import com.github.fge.jsonpatch.JsonPatch;
import lombok.AccessLevel;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/user-merchant/items")
public class ItemsController {

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    ItemsService itemsService;

    @GetMapping("/")
    public ResponseEntity<List<Item>> getAllItems(@RequestParam(required = false, defaultValue = "1") int page, @RequestParam(required = false, defaultValue = "10") int limit) {
        return ResponseEntity.ok(itemsService.getAllItems((page - 1) * limit, limit));
    }

    @PostMapping("/")
    public ResponseEntity<Void> addItem(@Valid @RequestBody AddItemRequest addItemRequest) {
        String id = itemsService.addItem(addItemRequest);
        URI location = URI.create(String.format("/user-merchant/items/%s/", id));
        return ResponseEntity.created(location).build();
    }

    @GetMapping("/{itemId}")
    public ResponseEntity<Item> getItemById(@PathVariable String itemId) {
        return ResponseEntity.ok(itemsService.getItemById(itemId));
    }

    @PatchMapping("/{itemId}")
    public ResponseEntity<Item> updateItemById(@PathVariable String itemId, @RequestBody UpdateItemRequest updateItem) throws IllegalAccessException {
        JsonPatch patch = PatchHelper.createJsonPatch(updateItem);
        itemsService.updateItemById(itemId, patch);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{itemId}")
    public ResponseEntity<Void> deleteItem(@PathVariable String itemId) {
        itemsService.deleteItem(itemId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{itemId}/game")
    public ResponseEntity<String> getGameByItemId(@PathVariable String itemId) {
        return ResponseEntity.ok(itemsService.getGameByItemId(itemId));
    }
}
