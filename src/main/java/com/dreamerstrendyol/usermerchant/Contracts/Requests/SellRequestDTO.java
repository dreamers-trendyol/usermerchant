package com.dreamerstrendyol.usermerchant.Contracts.Requests;

import lombok.Getter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
public class SellRequestDTO {
    @NotNull
    @Length(min = 3,max = 36)
    private String userId;
    @NotNull
    @Length(min = 3,max = 36)
    private String itemId;
    @NotNull
    @Min(value = 1)
    private double itemPrice;
}
