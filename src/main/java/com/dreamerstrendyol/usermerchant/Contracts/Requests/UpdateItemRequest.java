package com.dreamerstrendyol.usermerchant.Contracts.Requests;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UpdateItemRequest {
    @NotNull
    @Length(min = 3, max = 36)
    public String itemName;
    @NotNull
    @Length(min = 3, max = 36)
    public String itemGameId;
    @NotNull
    public String itemIcon;
    @NotNull
    @Length(min = 3, max = 250)
    public String itemDescription;
}
