package com.dreamerstrendyol.usermerchant.Contracts.Requests;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class AddItemRequest {
    @NotNull
    @Length(min = 3,max = 36)
    private String itemName;
    @NotNull
    @Length(min = 3,max = 36)
    private String itemGameId;
    @NotNull
    private String itemIcon;
    @NotNull
    @Length(min = 3,max = 250)
    private String itemDescription;

}
