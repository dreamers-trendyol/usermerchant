package com.dreamerstrendyol.usermerchant.Helper;

import com.dreamerstrendyol.usermerchant.Exceptions.UserDoesNotHaveTheItem;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

public class RestHelper {

    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    public Double getBalanceFromUserMicroservice(String userId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        Double balanceOfUser = this.getRestTemplate().exchange("http://users.cloudns.cl:8080/v1/user/" + userId + "/balance",
                HttpMethod.GET, entity, Double.class).getBody();
        return balanceOfUser;
    }

    public Boolean isUserCanListedItem(String itemId, String userId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        try {
            ResponseEntity<String> isUserHaveTheItem = this.getRestTemplate().exchange("http://users.cloudns.cl:8080/v1/user/" + userId + "/inventory/" + itemId,
                    HttpMethod.GET, entity, String.class);

            if (isUserHaveTheItem.getStatusCode().value() == 200) {
                JSONObject jsonResponse = new JSONObject(isUserHaveTheItem.getBody());
                return jsonResponse.getBoolean("canListedAtUserMerchant");
            }
        } catch (HttpClientErrorException e) {
            throw new UserDoesNotHaveTheItem();
        }
        return false;
    }


}
