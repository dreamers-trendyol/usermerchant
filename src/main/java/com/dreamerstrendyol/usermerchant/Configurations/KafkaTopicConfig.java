package com.dreamerstrendyol.usermerchant.Configurations;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {
    @Bean
    public NewTopic userListedItem() {
        return TopicBuilder.name("userListedItem").build();
    }

    @Bean
    public NewTopic userBoughtAnItem() {
        return TopicBuilder.name("userBoughtAnItem").build();
    }

    @Bean
    public NewTopic userDeleteListedItem() {
        return TopicBuilder.name("userDeleteListedItem").build();
    }


}
