package com.dreamerstrendyol.usermerchant.Configurations;

import com.couchbase.client.java.Cluster;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class CouchbaseIndexConfiguration {

    private final Cluster couchbaseCluster;

    public CouchbaseIndexConfiguration(Cluster couchbaseCluster) {
        this.couchbaseCluster = couchbaseCluster;
    }

    @Bean
    public void createIndexes() {
        try{
            couchbaseCluster.query("CREATE INDEX itemId ON merchant(itemId);");
            couchbaseCluster.query("CREATE INDEX itemPrice ON merchant(itemPrice);");
            couchbaseCluster.query("CREATE INDEX itemStatus ON merchant(itemStatus);");
            couchbaseCluster.query("CREATE INDEX ownerId ON merchant(ownerId);");
            couchbaseCluster.query("CREATE INDEX sellingId ON merchant(sellingId);");
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}

