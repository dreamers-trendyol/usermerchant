package com.dreamerstrendyol.usermerchant.Services;

import com.dreamerstrendyol.usermerchant.Contracts.Requests.AddItemRequest;
import com.dreamerstrendyol.usermerchant.Models.Item;
import com.github.fge.jsonpatch.JsonPatch;

import java.util.List;

public interface ItemsService {
    Item getItemById(String id);
    String getGameByItemId(String itemId);
    String addItem(AddItemRequest addItemRequest);
    void deleteItem(String id);
    Item updateItemById(String itemId, JsonPatch patch);
    List<Item> getAllItems(int i, int limit);
}
