package com.dreamerstrendyol.usermerchant.Services;

import com.dreamerstrendyol.usermerchant.Contracts.Requests.SellRequestDTO;
import com.dreamerstrendyol.usermerchant.Exceptions.BalanceIsNotEnoughtForOperation;
import com.dreamerstrendyol.usermerchant.Exceptions.TryingToBuySoldItemOnMerchant;
import com.dreamerstrendyol.usermerchant.Exceptions.TryingToDeleteSoldItemOnMerchant;
import com.dreamerstrendyol.usermerchant.Exceptions.TryingToSellItemButAlreadyListed;
import com.dreamerstrendyol.usermerchant.Helper.RestHelper;
import com.dreamerstrendyol.usermerchant.Models.ItemStatuses;
import com.dreamerstrendyol.usermerchant.Models.KafkaSender;
import com.dreamerstrendyol.usermerchant.Models.MarketItem;
import com.dreamerstrendyol.usermerchant.Repositories.UserMerchantRepository;
import lombok.AccessLevel;
import lombok.Setter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserMerchantServiceImpl implements UserMerchantService {

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    KafkaSender kafkaSender;

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    UserMerchantRepository userMerchantRepository;

    @Override
    public void buyItemFromMerchant(String itemId, String itemSellingId, String userId) {
        MarketItem marketItemForBuying = userMerchantRepository.getMarketItemFromMerchantWithVersion(itemSellingId);

        Double balanceFromUserMicroservice = new RestHelper().getBalanceFromUserMicroservice(userId);

        if (marketItemForBuying.getItemStatus() != ItemStatuses.SOLD) {
            if (balanceFromUserMicroservice < marketItemForBuying.getItemPrice()) {
                throw new BalanceIsNotEnoughtForOperation();
            }
            marketItemForBuying.makeMarketItemSold();

            JSONObject userBoughtMarketItemMessage = new JSONObject();
            userBoughtMarketItemMessage.put("itemId", itemId);
            userBoughtMarketItemMessage.put("userId", userId);
            userBoughtMarketItemMessage.put("ownerUserId", marketItemForBuying.getOwnerId());
            userBoughtMarketItemMessage.put("itemPrice", marketItemForBuying.getItemPrice());

            userMerchantRepository.boughtMarketItemFromMerchant(marketItemForBuying);

            kafkaSender.sendMessage(userBoughtMarketItemMessage.toString(), "userBoughtAnItem");
        } else {
            throw new TryingToBuySoldItemOnMerchant();
        }
    }

    @Override
    public void sellItemOnMerchant(String itemId, SellRequestDTO sellRequestDTO) {
        Boolean isUserCanListedItem = new RestHelper().isUserCanListedItem(sellRequestDTO.getItemId(), sellRequestDTO.getUserId());

        if (isUserCanListedItem) {
            MarketItem marketItemGoingToListed = new MarketItem(sellRequestDTO.getItemId(), sellRequestDTO.getItemPrice(), sellRequestDTO.getUserId());
            userMerchantRepository.addMarketItemToMerchantForListing(marketItemGoingToListed);
            JSONObject request = new JSONObject();
            request.put("itemId", sellRequestDTO.getItemId());
            request.put("userId", sellRequestDTO.getUserId());
            kafkaSender.sendMessage(request.toString(), "userListedItem");
        } else {
            throw new TryingToSellItemButAlreadyListed();
        }
    }

    @Override
    public List<MarketItem> getItemMerchant(String itemId, String filter) {
        List<MarketItem> itemMerchant;
        if (filter.equals("in-sale")) {
            itemMerchant = userMerchantRepository.getItemMerchant(itemId, ItemStatuses.IN_SALE);
        } else {
            itemMerchant = userMerchantRepository.getItemMerchant(itemId, ItemStatuses.SOLD);
        }
        return itemMerchant;
    }

    @Override
    public String getOwnerIdBySellingId(String itemId, String itemSellingId) {
        MarketItem marketItemFromMerchant = userMerchantRepository.getMarketItemFromMerchant(itemSellingId);
        return marketItemFromMerchant.getOwnerId();
    }

    @Override
    public MarketItem getItemIdBySellingId(String itemId, String itemSellingId) {
        return userMerchantRepository.getMarketItemFromMerchant(itemSellingId);
    }

    @Override
    public void deleteItemIdBySellingId(String itemId, String itemSellingId) {
        MarketItem marketItemFromMerchant = userMerchantRepository.getMarketItemFromMerchant(itemSellingId);
        if (marketItemFromMerchant.getItemStatus().equals(ItemStatuses.IN_SALE)) {
            JSONObject deleteMessage = new JSONObject();
            deleteMessage.put("userId", marketItemFromMerchant.getOwnerId());
            deleteMessage.put("itemId", itemId);
            kafkaSender.sendMessage(deleteMessage.toString(), "userDeleteListedItem");
            userMerchantRepository.deleteItemIdBySellingId(itemSellingId);
        } else {
            throw new TryingToDeleteSoldItemOnMerchant();
        }
    }

    @Override
    public List<MarketItem> getMarketWithFilter(String filter) {
        switch (filter) {
            case "popular":
                return userMerchantRepository.getMerchantPopularList();
            case "best-sellers":
                return userMerchantRepository.getMerchantBestSeller();
        }
        return new ArrayList<>();
    }
}
