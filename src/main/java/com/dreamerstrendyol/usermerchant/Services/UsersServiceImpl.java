package com.dreamerstrendyol.usermerchant.Services;

import com.dreamerstrendyol.usermerchant.Models.ItemStatuses;
import com.dreamerstrendyol.usermerchant.Models.MarketItem;
import com.dreamerstrendyol.usermerchant.Repositories.UserMerchantRepository;
import lombok.AccessLevel;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {
    @Autowired
    @Setter(AccessLevel.PACKAGE)
    UserMerchantRepository userMerchantRepository;

    public List<MarketItem> getUserListedItemsOnMerchant(String userId, String filter) {
        if (filter.equals("in-sale")) {
            return userMerchantRepository.getUserListedItemsONMerchant(userId, ItemStatuses.IN_SALE);
        } else {
            return userMerchantRepository.getUserListedItemsONMerchant(userId, ItemStatuses.SOLD);
        }
    }

}
