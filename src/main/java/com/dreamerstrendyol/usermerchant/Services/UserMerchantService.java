package com.dreamerstrendyol.usermerchant.Services;

import com.dreamerstrendyol.usermerchant.Contracts.Requests.SellRequestDTO;
import com.dreamerstrendyol.usermerchant.Models.MarketItem;

import java.util.List;

public interface UserMerchantService {
    void buyItemFromMerchant(String itemId, String itemSellingId, String userId);
    void sellItemOnMerchant(String itemId, SellRequestDTO sellRequestDTO);
    List<MarketItem> getItemMerchant(String itemId, String filter);
    String getOwnerIdBySellingId(String itemId, String itemSellingId);
    MarketItem getItemIdBySellingId(String itemId, String itemSellingId);
    void deleteItemIdBySellingId(String itemId, String itemSellingId);
    List<MarketItem> getMarketWithFilter(String filter);
}
