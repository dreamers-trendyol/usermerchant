package com.dreamerstrendyol.usermerchant.Services;

import com.dreamerstrendyol.usermerchant.Models.MarketItem;

import java.util.List;

public interface UsersService {
    List<MarketItem> getUserListedItemsOnMerchant(String userId, String filter);
}
