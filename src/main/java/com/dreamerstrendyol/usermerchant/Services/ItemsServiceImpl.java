package com.dreamerstrendyol.usermerchant.Services;

import com.dreamerstrendyol.usermerchant.Contracts.Requests.AddItemRequest;
import com.dreamerstrendyol.usermerchant.Helper.PatchHelper;
import com.dreamerstrendyol.usermerchant.Models.Item;
import com.dreamerstrendyol.usermerchant.Repositories.ItemsRepository;
import com.github.fge.jsonpatch.JsonPatch;
import lombok.AccessLevel;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemsServiceImpl implements ItemsService {

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    ItemsRepository itemsRepository;

    @Override
    @Cacheable(value = "cacheItemContainer", key = "#id")
    public Item getItemById(String id) {
        return itemsRepository.getItemById(id);
    }

    @Override
    public String getGameByItemId(String itemId) {
        return itemsRepository.getItemById(itemId).getItemGameId();
    }

    @Override
    public String addItem(AddItemRequest addItemRequest) {
        Item item = new Item(addItemRequest.getItemName(), addItemRequest.getItemGameId(), addItemRequest.getItemIcon(),
                addItemRequest.getItemDescription());
        return itemsRepository.addItem(item);
    }

    @Override
    @CacheEvict(value = "cacheItemContainer", key = "#id")
    public void deleteItem(String id) {
        itemsRepository.deleteItem(id);
    }

    @Override
    @CachePut(value = "cacheItemContainer", key = "#itemId")
    public Item updateItemById(String itemId, JsonPatch patch) {
        Item itemById = itemsRepository.getItemById(itemId);
        Item itemPatched = null;
        try {
            itemPatched = PatchHelper.applyPatch(patch, itemById);
            itemsRepository.updateItem(itemPatched);
        } catch (Exception e) {

        }
        return itemPatched;
    }

    @Override
    public List<Item> getAllItems(int i, int limit) {
        return itemsRepository.getAllItems(i, limit);
    }
}
