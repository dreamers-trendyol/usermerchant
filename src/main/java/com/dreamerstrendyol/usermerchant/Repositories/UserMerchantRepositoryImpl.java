package com.dreamerstrendyol.usermerchant.Repositories;

import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.kv.GetResult;
import com.couchbase.client.java.query.QueryResult;
import com.dreamerstrendyol.usermerchant.Models.ItemStatuses;
import com.dreamerstrendyol.usermerchant.Models.MarketItem;
import lombok.AccessLevel;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.couchbase.client.java.kv.ReplaceOptions.replaceOptions;

@Repository
public class UserMerchantRepositoryImpl implements UserMerchantRepository {

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    private Cluster couchbaseCluster;

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    private Collection userMerchantCollection;

    @Override
    public List<MarketItem> getItemMerchant(String itemId, ItemStatuses itemStatus) {
        String statement = String.format("Select itemId,sellingId,itemPrice,ownerId,itemStatus,META().cas as version from merchant where itemId = \"%s\" AND itemStatus= \"%s\" ORDER BY itemPrice ASC", itemId, itemStatus);
        QueryResult queryResult = couchbaseCluster.query(statement);
        System.out.println(queryResult);
        return queryResult.rowsAs(MarketItem.class);
    }

    @Override
    public void addMarketItemToMerchantForListing(MarketItem item) {
        userMerchantCollection.insert(item.getSellingId(), item);
    }

    @Override
    public MarketItem getMarketItemFromMerchant(String marketItemSellingId) {
        GetResult getResult = userMerchantCollection.get(marketItemSellingId);
        return getResult.contentAs(MarketItem.class);
    }

    @Override
    public MarketItem getMarketItemFromMerchantWithVersion(String marketItemSellingId) {
        MarketItem marketItem = null;
        try {
            String statement = String.format("SELECT itemId,sellingId,itemPrice,ownerId,itemStatus,META().cas as version FROM merchant WHERE sellingId = \"%s\"", marketItemSellingId);
            QueryResult queryResult = couchbaseCluster.query(statement);
            marketItem = queryResult.rowsAs(MarketItem.class).get(0);
        } catch (IndexOutOfBoundsException e) {
            throw new NullPointerException();
        }
        return marketItem;
    }

    @Override
    public void boughtMarketItemFromMerchant(MarketItem item) {
        userMerchantCollection.replace(item.getSellingId(), item, replaceOptions().cas(item.getVersion()));
    }

    @Override
    public void deleteItemIdBySellingId(String itemSellingId) {
        userMerchantCollection.remove(itemSellingId);
    }

    @Override
    public List<MarketItem> getUserListedItemsONMerchant(String userId, ItemStatuses itemStatus) {
        String statement = String.format("Select itemId,sellingId,itemPrice,owner,itemStatus from merchant where itemId = \"%s\" AND itemStatus= \"%s\" ORDER BY itemPrice ASC", userId, itemStatus);
        QueryResult queryResult = couchbaseCluster.query(statement);
        System.out.println(queryResult);
        return queryResult.rowsAs(MarketItem.class);
    }

    @Override
    public List<MarketItem> getMerchantPopularList() {
        String statement = String.format("Select count(*) as count,itemId from merchant WHERE itemStatus==\"%s\" GROUP BY itemId ORDER BY count desc;", ItemStatuses.IN_SALE);
        QueryResult queryResult = couchbaseCluster.query(statement);
        System.out.println(queryResult);
        return queryResult.rowsAs(MarketItem.class);
    }

    @Override
    public List<MarketItem> getMerchantBestSeller() {
        String statement = String.format("Select count(*) as count,itemId from merchant WHERE itemStatus==\"%s\" GROUP BY itemId ORDER BY count desc;", ItemStatuses.SOLD);
        QueryResult queryResult = couchbaseCluster.query(statement);
        System.out.println(queryResult);
        return queryResult.rowsAs(MarketItem.class);
    }
}
