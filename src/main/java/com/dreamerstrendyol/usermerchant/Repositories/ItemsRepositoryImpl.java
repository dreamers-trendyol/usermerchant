package com.dreamerstrendyol.usermerchant.Repositories;

import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.kv.GetResult;
import com.couchbase.client.java.query.QueryResult;
import com.dreamerstrendyol.usermerchant.Models.Item;
import lombok.AccessLevel;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ItemsRepositoryImpl implements ItemsRepository {
    @Autowired
    @Setter(AccessLevel.PACKAGE)
    private Cluster couchbaseCluster;

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    private Collection itemCollection;

    @Override
    public List<Item> getAllItems(int offset, int limit) {
        String statement = String.format("SELECT itemId, itemName, itemGameId, itemIcon, itemDescription FROM items OFFSET %d LIMIT %d", offset, limit);
        QueryResult queryResult = couchbaseCluster.query(statement);
        return queryResult.rowsAs(Item.class);
    }

    @Override
    public Item getItemById(String id) {
        GetResult getResult = itemCollection.get(id);
        return getResult.contentAs(Item.class);
    }

    @Override
    public String addItem(Item item) {
        itemCollection.insert(item.getItemId(), item);
        return item.getItemId();
    }

    @Override
    public void deleteItem(String id) {
        itemCollection.remove(id);
    }

    @Override
    public void updateItem(Item item) {
        itemCollection.replace(item.getItemId(), item);
    }

}
