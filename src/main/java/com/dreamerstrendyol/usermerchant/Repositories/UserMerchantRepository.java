package com.dreamerstrendyol.usermerchant.Repositories;

import com.dreamerstrendyol.usermerchant.Models.ItemStatuses;
import com.dreamerstrendyol.usermerchant.Models.MarketItem;

import java.util.List;

public interface UserMerchantRepository {
    List<MarketItem> getItemMerchant(String itemId, ItemStatuses itemStatus);
    void addMarketItemToMerchantForListing(MarketItem item);
    MarketItem getMarketItemFromMerchant(String marketItemSellingId);
    MarketItem getMarketItemFromMerchantWithVersion(String marketItemSellingId);
    void boughtMarketItemFromMerchant(MarketItem item);
    void deleteItemIdBySellingId(String itemSellingId);
    List<MarketItem> getUserListedItemsONMerchant(String userId, ItemStatuses itemStatus);
    List<MarketItem> getMerchantPopularList();
    List<MarketItem> getMerchantBestSeller();
}
