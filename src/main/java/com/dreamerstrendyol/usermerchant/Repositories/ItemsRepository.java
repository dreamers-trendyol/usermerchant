package com.dreamerstrendyol.usermerchant.Repositories;

import com.dreamerstrendyol.usermerchant.Models.Item;

import java.util.List;

public interface ItemsRepository {
    List<Item> getAllItems(int offset, int limit);
    Item getItemById(String id);
    String addItem(Item item);
    void deleteItem(String id);
    void updateItem(Item item);
}
